use personal_website::ThreadPool;
use std::{
    fs,
    io::{prelude::*, BufReader},
    net::{TcpListener, TcpStream},
};

#[derive(Debug)]
enum ConnectionHandleError {
    ReadFile(std::io::Error),
    WriteResponse(std::io::Error),
    MissingHeader,
    InvalidHeader(std::io::Error),
}

fn main() {
    let listener = TcpListener::bind("localhost:6969").unwrap();
    let pool = ThreadPool::new(4);

    for stream in listener.incoming() {
        let stream = match stream {
            Ok(v) => v,
            Err(e) => {
                eprintln!("Invalid stream: {e}, continuing");
                continue;
            }
        };
        pool.execute(|| {
            handle_connection(stream).unwrap_or_else(|err| {
                eprintln!("Failed to handle connection: {:?}, continuing", err);
            });
        });
    }

    println!("Shutting down.");
}

fn handle_connection(mut stream: TcpStream) -> Result<(), ConnectionHandleError> {
    let buf_reader = BufReader::new(&mut stream);
    let request_line = match buf_reader.lines().next() {
        Some(request_line) => request_line.map_err(ConnectionHandleError::InvalidHeader)?,
        None => return Err(ConnectionHandleError::MissingHeader),
    };

    println!("{request_line}");

    let (status_line, file_name, content_type) = match &request_line[..] {
        "GET / HTTP/1.1" => ("HTTP/1.1 200 OK", "out/index.html", "text/html"),
        "GET /style.css HTTP/1.1" => ("HTTP/1.1 200 OK", "out/style.css", "text/css"),
        "GET /tetris HTTP/1.1" => ("HTTP/1.1 200 OK", "out/tetris.html", "text/html"),
        "GET /tetris.js HTTP/1.1" => ("HTTP/1.1 200 OK", "out/tetris.js", "text/javascript"),
        "GET /favicon.ico HTTP/1.1" => (
            "HTTP/1.1 200 OK",
            "out/favicon.ico",
            "image/vnd.microsoft.icon",
        ),
        "GET /tetris_bg.wasm HTTP/1.1" => {
            ("HTTP/1.1 200 OK", "out/tetris_bg.wasm", "application/wasm")
        }
        "GET /assets/fonts/FiraMono-Medium.ttf HTTP/1.1" => (
            "HTTP/1.1 200 OK",
            "out/assets/fonts/FiraMono-Medium.ttf",
            "font/ttf",
        ),
        "GET /assets/fonts/FiraMono-Bold.ttf HTTP/1.1" => (
            "HTTP/1.1 200 OK",
            "out/assets/fonts/FiraMono-Bold.ttf",
            "font/ttf",
        ),
        "GET /assets/fonts/FiraSans-Bold.ttf HTTP/1.1" => (
            "HTTP/1.1 200 OK",
            "out/assets/fonts/FiraSans-Bold.ttf",
            "font/ttf",
        ),
        _ => ("HTTP/1.1 404 NOT FOUND", "out/404.html", "text/html"),
    };

    println!("Downloading file: {file_name}");

    let contents = fs::read(file_name).map_err(ConnectionHandleError::ReadFile)?;

    let length = contents.len();

    let response = format!(
        "{status_line}\r\nContent-Length: {length}\r\nContent-Type: {content_type}\r\n\r\n"
    );
    let response: &[u8] = &[response.as_bytes(), &contents].concat();

    stream
        .write_all(response)
        .map_err(ConnectionHandleError::WriteResponse)?;
    Ok(())
}
