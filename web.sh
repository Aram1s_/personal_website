#!/bin/sh
cd tetris || exit
cargo build --profile web-release --target wasm32-unknown-unknown
wasm-bindgen --no-typescript --target web \
	--out-dir ./out/ \
	--out-name "tetris" \
	./target/web-release/tetris.wasm

wasm-opt -Oz -o out/tetris_bg.wasm out/tetris_bg.wasm
cd ..
cp tetris/out/* out/
cargo run --release
